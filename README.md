# 客制化Live-os（Kodachi）

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;前段时间接触到了Linux的[Live-os](https://wiki.ubuntu.com.cn/LiveCD) ，特点是用户在使用期间产生的文件断电即丢失，对系统文件的操作也是断电即恢复初始状态，对保护用户的隐私有很大作用。电脑自带硬盘也是只读的状态（开机后插入的硬盘和移动硬盘除外），不插入外置硬盘的状态下能抵御大部分勒索病毒对硬盘内文件的加密。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;但是我在使用Live-os的时候遇到了很多问题，比如 在使用 **[Tails（基于 Debian 10 (Buster)）](https://staging.tails.boum.org/index.zh.html)** 的时候，官方已经支持UEFI启动，但在我电脑上以UEFI的方式无法启动（查阅资料后发现可能是独立显卡的原因），用传统的BIOS方式就可以启动（Secure boot也已关闭）。无奈，遂寻找其他出名的以保护隐私为口号的Live-os，又找到了另一保护隐私的Live-os： **[Kodachi（基于Xubuntu 18.04 LTS ）](https://www.digi77.com/linux-kodachi/)**（Kodachi的grub菜单有禁用独立显卡启动的选项，经测试后，系统可以正常启动，不禁用无法启动），作者是阿拉伯国家阿曼人。Kodachi的初衷就是保护隐私，作者在Twitter上也提到[#Kodachi is not for Hacking :)](https://twitter.com/warith2020/status/1243867404586881026) 。不同的Linux发行版有不同的特点，比如Kali就是专门用于网络安全方向。


&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kodachi跟Tails有很大的区别（[Kodachi官网也有对比图](https://www.digi77.com/linux-kodachi/) ），其中一点是没有中文支持，刚开始用起来还是比较费劲的。经过查阅大量的资料，还是通过重新打包ISO镜像/casper目录下的filesystem.squashfs，实现了客制化的Kodachi（部分内容对tails也适用）。由于打包的内容会根据我的需求来定制，不具有通用性，所以打包后的ISO镜像不会发出。打包的流程都是一样的，就是内容不一样，根据自己的喜好来添加内容。

**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kodachi的桌面面板会显示BTC，XMR的价格和BTC捐赠数量，导致国内有人认为其利用自己的电脑挖矿。其实显示的BTC,XMR的价格和BTC捐赠数量是从Kodachi官网的一个[json](https://www.digi77.com/software/kodachi/Kodachi.json) 文件提取的，仅用于显示。加载BTC,XMR价格和BTC捐赠数量的两个函数在Kodachi系统的 /home/kodachi/.kbase/systemhealth  脚本里。6.2版本直接在用户的电脑里请求三个地址 获取[BTC]([1](https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=usd)) ,[XMR](https://api.coingecko.com/api/v3/simple/price?ids=monero&vs_currencies=usd) 的价格和[BTC捐赠的数量]([2](blockchain.info/rawaddr/19pufEJUkQGPJYXCfA1b7GaKSUNeYEsApU) ) （[6.2版本BTC&XMR的查询函数](https://github.com/WMAL/kodachi/blob/master/home/.kbase/systemhealth#L1582) ），可能作者觉得不太合适，之后的版本才从官网的json文件里提取。**

7.1版本的两个查询函数如下：


		function getBtCprice()
		{
		        #Get btc price
		        #spare parts
		        #curl -s https://www.btcturk.com/api/ticker|jq -r '.[5].last'
		
		        btcValue=$(sudo curl -s -m 20 -X GET "$Remote_Jason_name" -H  "accept: application/json" |jq -r '.ServerFeed1[].btcPrice'|xargs);
		        writeToJason "$btcValue" "btcprice";
		        xmrValue=$(sudo curl -s -m 20 -X GET "$Remote_Jason_name" -H  "accept: application/json" |jq -r '.ServerFeed1[].xmrPrice'|xargs);
		        writeToJason "$xmrValue" "xmrprice";
		}
		
		
		function getBTCDonations()
		{
		
		        btcDonationValue=$(sudo curl -s -m 20 -X GET "$Remote_Jason_name" -H  "accept: application/json" |jq -r '.ServerFeed1[].walletBlance'|xargs);
		        writeToJason "$btcDonationValue" "btcbalance";
		}

`$Remote_Jason_name`的值为：“https://www.digi77.com/software/kodachi/Kodachi.json”， 直接在Kodachi终端`echo $Remote_Jason_name`就能看到。

**目前打包的内容：**

1. 编译安装了中文man page[（项目地址）](https://github.com/man-pages-zh/manpages-zh) ，并将其man命令替换为cman（echo "alias cman='man -M /usr/local/share/man/zh_CN'>>/home/kodachi/.bashrc），以实现在英文操作系统下同时使用中英两种语言的man page（中文为cman，英文为man）。其实也可以用 apt安装manpages-zh，但是Ubuntu 18.04 LTS源里面的版本为1.6.3.2-1(2017)，manpages-zh目前releases的最新版本为1.6.3.4(2020)，两个版本相差了三年，遂编译安装。

2. 安装了目前最新的安全补丁（基于Ubuntu 18.04 LTS源）。由于Kodachi的版本发布间隔比较长，不能及时的与Ubuntu源进行同步，所以安全补丁也不能及时更新。

   安装安全补丁：

		
		sudo apt update
		sudo apt list --upgradable | grep -e "-security" | awk -F "/" '{print $1}' | xargs apt install



3. 其他程序的增加：wireshark(流量分析工具)，adb&fastboot(安卓手机调试工具)，ibus-libpinyin(中文输入法)，只安装必要的程序，以减少攻击面。

4. 添加了SSH登陆时的验证&加密密钥

   在Kodachi最新版（7.1）启动SSH服务时，报出了这三个错

		Could not load host key: /etc/ssh/ssh_host_rsa_key
		Could not load host key: /etc/ssh/ssh_host_ecdsa_key
		Could not load host key: /etc/ssh/ssh_host_ed25519_key

原因是因为原作者没有添加SSH登陆时用于C/S通信加密&验证的密钥对（在/etc/rc2.d,rc3.d,rc4.d,rc5.d中的K01ssh脚本也没有发现关于生成密钥的命令），导致ssh登陆过程中无法完成密钥交换这一环节。

解决方法：
	
使用 `ssh-keygen -A`命令添加了rsa，dsa，ecdsa和ed25519密钥,密码为空。

-A参数的具体作用移步[https://man.openbsd.org/ssh-keygen.1](https://man.openbsd.org/ssh-keygen.1)， 查看DESCRIPTION部分的解释
### 一.重新打包的准备

**客制化操作就是对ISO镜像里面的filesystem.squashfs进行修改后重新打包**

下面是wikipedia对[SquashFS](https://zh.wikipedia.org/wiki/SquashFS)的介绍 ：
	SquashFS
	维基百科，自由的百科全书
		
	Squashfs（.sfs）是一套供Linux核心使用的GPL開源只讀壓縮檔案系統。Squashfs能夠為檔案系統內的檔案、inode及目錄結構進行壓縮，並支援最大1024千位元組的區段，以提供更大的壓縮比。 
	Squashfs的設計是專門為一般的只讀檔案系統的使用而設計，它可應用於數據備份，或是系统资源紧张的電腦上使用。最初版本的Squashfs採用 gzip 的數據壓縮。版本 2.6.34 之后的Linux内核增加了对 LZMA和 LZO 压缩算法的支持，版本 2.6.38 的内核增加了对LZMA2的支持，该算法同时也是xz使用的压缩算法。
	
	用途
	Squashfs常被用于各Linux发行版的LiveCD中，也用于OpenWrt 和DD-WRT 的路由器固件。Chromecast也是该文件系统的用户。 
	
Waiting next commit..